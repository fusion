/*
 * -| FUSION |-
 * The FUSION Linux Hook for Tremulous SVN
 * Author: Source <fusion.devel@gmail.com>
 * Date: April 09,2007
 * License: LGPL-2
 *
 * Credits:
 *   ascii (*C)
 *   h1web (*C)
 */

#include <stdio.h>
#include <stdarg.h>
#include <stdlib.h>
#include <string.h>

#define DIE_STR "[FUSION][FATAL]: "
#define LOG_STR "[FUSION][LOG]: "
#define STD_STR "[FUSION]: "

extern FILE *logfile;

/* Printf handlers */
void diemsg(char *fmt, ...) {
  va_list ap;
  va_start(ap, fmt);
  fprintf(stderr, DIE_STR);
  vfprintf(stderr, fmt, ap);
  fprintf(stderr,"\n");
  va_end(ap);
  exit(1);
}

void logmsg(char *fmt, ...) {
  va_list ap;
  va_start(ap, fmt);
  fprintf(logfile, LOG_STR);
  vfprintf(logfile, fmt, ap);
  fprintf(logfile, "\n");
  va_end(ap);
}

void stdmsg(char *fmt, ...) {
  va_list ap;
  va_start(ap, fmt);
  fprintf(stdout, STD_STR);
  vfprintf(stdout, fmt, ap);
  fprintf(stdout, "\n");
  va_end(ap);
}
