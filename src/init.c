/*
 * -| FUSION |-
 * The FUSION Linux Hook for Tremulous SVN
 * Author: Source <fusion.devel@gmail.com>
 * Date: April 09,2007
 * License: LGPL-2
 *
 * Credits:
 *   ascii (*C)
 *   h1web (*C)
 */
#define _GNU_SOURCE
#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <dlfcn.h>

#include "src/qcommon/vm_local.h"
#include "src/cgame/cg_local.h"

#define MAX_VM 3
vm_t* vmTable;
cg_t *ocg = NULL;
cgs_t *ocgs = NULL;
FILE *logfile = NULL;

void* scanthread(void *arg) {
  unsigned int i;
  while(1) {
    for(i = 0; i < MAX_VM; i++) {
      if(!strcmp(vmTable[i].name, "cgame") && vmTable[i].systemCall) {
        hookvm(&vmTable[i]);
      }
    }
    sleep(1);
  }
}

__attribute__ ((constructor))
void fusionCore_Init(void) {
  char path[1024];
  unsigned long size; 
  void *handle;
 
  logfile = fopen("/tmp/fusioncore.tmp", "w");
  if(!logfile) {
    diemsg("Cannot open log file.");
  }
  memset(path, 0, sizeof(path));
  readlink("/proc/self/exe", path, sizeof(path));

  if(!strstr(path, "tremulous")) {
    stdmsg("Injected into foreign process, not hooking.");
    return;
  }
  
  handle = (void *)mapfile(path, &size);
  initimports(handle);
  if(vmTable = (vm_t*)lookup(handle, "vmTable")) {
    pthread_t pt;
    stdmsg("Got vmTable (0x%x) Launching scanthread.", vmTable);
    pthread_create(&pt, 0, scanthread, 0);
  }
  munmap(handle,size);
  return;
}

__attribute__ ((destructor))
void fusionCore_Exit(void) {
  fflush(logfile);
  fclose(logfile);
}

__attribute__ ((visibility ("default")))
void *memset(void *addr, int fill, size_t size) {
  typedef void *(*memset_t)(void *addr, int fill, size_t size);
  static memset_t orig_memset = NULL;
  cg_t *tmpcg = 0;
  cgs_t *tmpcgs = 0;

  if(!orig_memset)
    orig_memset = (memset_t)dlsym(RTLD_NEXT, "memset");

  if(size == sizeof(cg_t)) {
    stdmsg("Game structure found! [ cg]:[0x%x]", addr);
    ocg = (void *)addr;
  } else
  if(size == sizeof(cgs_t)) {
    stdmsg("Game structure found! [cgs]:[0x%x]", addr );
    ocgs = (void *)addr;
  }

  return orig_memset(addr, fill, size);
}
