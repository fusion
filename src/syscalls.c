/*
 * -| FUSION |-
 * The FUSION Linux Hook for Tremulous SVN
 * Author: Source <fusion.devel@gmail.com>
 * Date: April 09,2007
 * License: LGPL-2
 *
 * Credits:
 *   ascii (*C)
 *   h1web (*C)
 */

#include "src/qcommon/vm_local.h"
#include "src/cgame/cg_local.h"
#include "_model.h"

extern cg_t  *ocg;
extern cgs_t *ocgs;

vm_t* cgvm = 0;
#define VMDATA(x) (cgvm->dataBase + (x & cgvm->dataMask))

typedef intptr_t (*systemCall_t)(intptr_t*);
systemCall_t _systemCall = 0;
int lastent = 0;

qhandle_t greenShader;
qhandle_t redShader;

typedef model_t* (*R_GetModelByHandle_t)(qhandle_t);
R_GetModelByHandle_t R_GetModelByHandle = 0;

intptr_t systemCall(intptr_t* parms) {
  if(parms[0] == CG_R_ADDREFENTITYTOSCENE) {
    refEntity_t* re = (refEntity_t*)VMDATA(parms[1]);
    if(re->reType == RT_SPRITE )
      re->shaderRGBA[3] = 0x20;
    if(lastent > 0 && lastent <= MAX_CLIENTS && !(re->renderfx & RF_THIRD_PERSON)) {
      re->renderfx = RF_DEPTHHACK|RF_MINLIGHT|RF_NOSHADOW;
      model_t* m = (*R_GetModelByHandle)(re->hModel);
      char *curmodel = m->name;
      if(strstr(curmodel, "models/players")) {
        re->customShader = greenShader;
      } 
    }
  }
  else if(parms[0] == CG_S_UPDATEENTITYPOSITION) {
    lastent = parms[1];
  }
  else if(parms[0] == CG_R_REGISTERSHADER) {
    char *name = VMDATA(parms[1]);
    qhandle_t shader = (*_systemCall)(parms);
    if(strstr(name, "greenbuild")) greenShader = shader; 
          else
    if(strstr(name, "redbuild")) redShader = shader;

    return shader;
  }

  intptr_t retval = (*_systemCall)(parms);
  return retval;
}

void hooksyscalls(vm_t* vm) {
  cgvm = vm;
  _systemCall = vm->systemCall;
  vm->systemCall = (systemCall_t)&systemCall;
}

void initimports(void *handle) {
  R_GetModelByHandle = (R_GetModelByHandle_t)lookup(handle, "R_GetModelByHandle");
}
