/*
 * Routines to simplify access to the ELF Header.
 * h1web <h1web@djbj.de> | http://henrik.unit5.ca
 *
 * Copyright 2007 Henrik Friedrichsen. All rights reserved.
 *
 * Redistribution and use in source and binary forms, 
 * with or without modification, are permitted provided that the following conditions are met:
 * Redistributions of source code must retain the above copyright notice, 
 * this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright notice, 
 * this list of conditions and the following disclaimer in the documentation 
 * and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY HENRIK FRIEDRICHSEN ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY 
 * AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
 * IN NO EVENT SHALL HENRIK FRIEDRICHSEN OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; 
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <stdio.h>
#include <link.h>
#include <elf.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <fcntl.h>


#ifdef __FreeBSD__
	#define ELF(x) Elf_##x
#else
	#define ELF(x) ElfW(x)
#endif

/*
 * you have to unmap the file yourself with munmap(handle, size)
 */

void*
mapfile(char* path, unsigned long* size)
{
	FILE* f;
	struct stat s;

	f = fopen(path, "r");
	fstat(fileno(f), &s);
	*size = s.st_size;
	
	return mmap(NULL, s.st_size, PROT_READ, MAP_PRIVATE, fileno(f), 0);
}

void*
lookup(void* base, char* name)
{
	unsigned int i, symcount;
	ELF(Ehdr)* e = base;
	ELF(Shdr)* shdr = (ELF(Shdr)*)((char*)e + e->e_shoff);
	ELF(Sym)* sym = 0;
	char* strtab = 0;

	for(i = 0; i < e->e_shnum; i++) {
		if(shdr->sh_type == SHT_SYMTAB) {
			sym = (ELF(Sym)*)((char*)e + shdr->sh_offset);
			symcount = shdr->sh_size / sizeof(ELF(Sym));
		}
		else if(shdr->sh_type == SHT_STRTAB)
			strtab = (char*)((char*)e + shdr->sh_offset);
		shdr++;
	}

	for(i = 0; i < symcount; i++) {
		if(!strcmp(&strtab[sym->st_name], name))
			return (void*)sym->st_value;
		sym++;
	}

	return 0;
}
