/*
 * -| FUSION |-
 * The FUSION Linux Hook for Tremulous SVN
 * Author: Source <fusion.devel@gmail.com>
 * Date: April 09,2007
 * License: LGPL-2
 *
 * Credits:
 *   ascii (*C)
 *   h1web (*C)
 */
#include "src/qcommon/vm_local.h"
#include "src/cgame/cg_local.h"

extern intptr_t systemCall(intptr_t* params);

void hookvm(vm_t* vm) {
  if(vm->systemCall != &systemCall)
    hooksyscalls(vm);
}

